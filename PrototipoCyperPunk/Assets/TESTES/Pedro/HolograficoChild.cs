﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HolograficoChild : MonoBehaviour {

	// Use this for initialization

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D (Collider2D col) {
		transform.parent.gameObject.SendMessage ("OnTriggerEnter2D", col);
	}

	void OnCollisionEnter2D(Collision2D col){
		transform.parent.gameObject.SendMessage ("OnCollisionEnter2D", col);
	}

	public void disableMe(){
		Global.PlayerAcess.transform.SetParent (null);
		gameObject.SetActive (false);
	}
}
