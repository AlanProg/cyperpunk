﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySFXScript : MonoBehaviour
{
    public static EnemySFXScript Instance;

    public AudioSource enemenemyshotsfx;
    public AudioClip CerberuscanonSound;
    public AudioClip ShotSound;
    public AudioClip JammerSound;
    public AudioClip GranadeSound;
    public AudioClip DeathSound;
    public AudioClip SelectionSound;
    public AudioClip MenuChange;

    void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("Existem Múltiplas Instancias do Script EnemySFXScript");
        }
        Instance = this;

    }

    public void MakeCerberuscanonSound()
    {
        MakeSound(CerberuscanonSound);
    }

    public void MakeShotSound()
    {
        MakeSound(ShotSound);
    }

    public void MakeJammerSound()
    {
        MakeSound(JammerSound);
    }

    public void MakeGranadeSound()
    {
        MakeSound(GranadeSound);
    }

    public void MakeDeathSound()
    {
        MakeSound(DeathSound);
    }

    public void MakeSelectionSound()
    {
        MakeSound(SelectionSound);
    }

    public void MakeMenuChangeSound()
    {
        MakeSound(MenuChange);

    }

    private void MakeSound(AudioClip originalClip)
    {
        AudioSource.PlayClipAtPoint(originalClip, transform.position);
    }
}
