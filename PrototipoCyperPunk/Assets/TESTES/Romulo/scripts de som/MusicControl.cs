﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MusicControl : MonoBehaviour
{
    public AudioMixerSnapshot Tier1A;
    public AudioMixerSnapshot Tier1B;
    public AudioMixerSnapshot Tier1C;
    public AudioMixerSnapshot Tier1D;
    public AudioMixerSnapshot Tier1E;
    public AudioMixerSnapshot Tier2A;
    public AudioMixerSnapshot Tier2B;
    public AudioMixerSnapshot Tier2C;
    public AudioMixerSnapshot Tier2D;
    public AudioMixerSnapshot Tier2E;
    public AudioMixerSnapshot Powerup;
    public AudioMixerSnapshot Poweruptier2;
    public AudioMixerSnapshot BossIntro;
    public AudioMixerSnapshot BossPattern1;
    public AudioMixerSnapshot BossPattern2;
    public AudioMixerSnapshot ApolloPattern1;
    public AudioMixerSnapshot ApolloPattern2;
    public AudioMixerSnapshot ApolloSpecial;
    public float bpm = 130;
    private float transition;
    private float qnote;


    // Use this for initialization
    void Start()
    {
        qnote = 60 / bpm;
        //medindo um quarto de uma batida da musica
        transition = qnote;
        //setando a transição pra um quarto de batida

    }

    // Update is called once per frame
    void Update()
    {
        //Alan vou precisar que voce monte essa sequencia de IFs conforme as variaveis forem colocadas, vou escrever aqui mais ou menos como vai ser 

		if (Global.Tema == 1)//variavel pra dizer que é uma sala do tier 1 nao boss e nao power up
        {
			if (Global.randomValue == 1 //variavel de randomização
                )
            {
                Tier1A.TransitionTo(transition);
            }
			if (Global.randomValue == 2 //variavel de randomização
                )
            {
                Tier1B.TransitionTo(transition);
            }
			if (Global.randomValue == 3 //variavel de randomização
                )
            {
                Tier1C.TransitionTo(transition);
            }
			if (Global.randomValue == 4 //variavel de randomização
                )
            {
                Tier1D.TransitionTo(transition);
            }
			if (Global.randomValue == 5 //variavel de randomização
                )
            {
                Tier1E.TransitionTo(transition);
            }
        }
		if (Global.Tema == 2//variavel pra dizer que é uma sala do tier 2 nao boss e nao power up
            )
        {
			if (Global.randomValue == 1 //variavel de randomização
                  )
            {
                Tier2A.TransitionTo(transition);
            }
			if (Global.randomValue == 2 //variavel de randomização
                )
            {
                Tier2B.TransitionTo(transition);
            }
			if (Global.randomValue == 3 //variavel de randomização
               )
            {
                Tier2C.TransitionTo(transition);
            }
			if (Global.randomValue == 4 //variavel de randomização
               )
            {
                Tier2D.TransitionTo(transition);
            }
			if (Global.randomValue == 5 //variavel de randomização
               )
            {
                Tier2E.TransitionTo(transition);
            }

        }
		if (Global.Tema == 3//variavel pra dizer que é um power up do tier 1
            )
        {
            Poweruptier2.TransitionTo(transition);
        }
		if (Global.Tema == 4//variavel pra dizer que é um power up do tier 2
            )
        {
            Powerup.TransitionTo(transition);
        }
		if (Global.Tema == 5//variavel pra dizer que a sala do cerbero
            )
        {
			if (Global.qual_metade_Cerb <= 0/4//variavel pra dizer que o cerbero foi derrotado ou nao iniciou a luta ainda
                )
            {
                BossPattern1.TransitionTo(transition);
            }
			if (Global.qual_metade_Cerb > 0/4 //variavel pra dizer que o cerbero foi derrotado ou nao iniciou a luta ainda
                )
            {
				if (Global.qual_metade_Cerb > 2 / 4 //variavel da vida do cerbero
                    )
                {
                    BossPattern1.TransitionTo(transition);
                }
				if (Global.qual_metade_Cerb <= 2 / 4 //variavel de vida do cerbero
                )
                {
                    BossPattern2.TransitionTo(transition);
                }
            }
        }
		if (Global.Tema == 6//fase do apolo 
            )
        {
			if (Global.qual_metade_APOLO == 0//variavel pra dizer que o apolo nao iniciou a luta ainda, deve continuar True.
                )
            {
                ApolloPattern1.TransitionTo(transition);
            }
			if (Global.qual_metade_APOLO > 0 //variavel pra dizer que o boss nao iniciou a luta ainda
                )
            {
				if (Global.qual_metade_APOLO > 2 //variavel da vida do boss
                    )
                {
					if (Global.Especial_Boss_APOLO < 0 //se apolo esta dando a sumon dele
                        )
                    {
                        ApolloPattern1.TransitionTo(transition);
                    }
					if (Global.Especial_Boss_APOLO > 0 //se apolo esta dando a sumon dele
                        )
                    {
                        ApolloSpecial.TransitionTo(transition);
                    }
                }
				if (Global.qual_metade_APOLO <= 2 //variavel da vida do boss
        )
                {

					if (Global.Especial_Boss_APOLO == 0 //se apolo esta dando a sumon dele
                        )
                    {
                        ApolloPattern2.TransitionTo(transition);
                    }
					if (Global.Especial_Boss_APOLO > 0 //se apolo esta dando a sumon dele
                        )
                    {
                        ApolloSpecial.TransitionTo(transition);
                    }


                }
            }

        }
    }
}
