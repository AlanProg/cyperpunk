﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Segredo : MonoBehaviour {

	[Header("Invisibilidade que terá ao ter o player sobre ele")]
	[Range(0,1)]
	public float invisibilidade;

	private SpriteRenderer sprite;
	private bool sobre;
	private Color newColor;

	void Start(){

		sprite = GetComponent<SpriteRenderer> ();

	}

	void Update(){

		if (sobre) {
			if (sprite.color.a > invisibilidade) {
				newColor = sprite.color;
				newColor.a -= 0.1f;
				sprite.color = newColor;
			}
		} else if (!sobre) {
			if (sprite.color.a < 1) {
				newColor = sprite.color;
				newColor.a += 0.1f;
				sprite.color = newColor;
			}
		}

	}

	void OnTriggerEnter2D(Collider2D col){
		if (col.gameObject.tag == "Player") {
			sobre = true;
		}
	}

	void OnTriggerExit2D(Collider2D col){
		if (col.gameObject.tag == "Player") {
			sobre = false;
		}
	}


}
