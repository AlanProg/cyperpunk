﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimento : MonoBehaviour
{


	public Transform[] destinos = new Transform[2];
	public Transform platTransf;
	public float velo;
	public int timerTrocaDestino;
	public int destino;
	private bool move, troca;

	public int direcao = 1;

	public bool isSpawner = false;
	public bool isPlayerDetecting = false;
	public bool isCiclico = false;
    public bool startInDest = true;
	public bool isJammerLocked = false;
	public bool DoesItReturn = true;

	private SpriteRenderer spr;

	// Use this for initialization
	void Start ()
	{
		spr = GetComponent<SpriteRenderer>();

		if (isCiclico && isSpawner) {
			print ("Favor lembrar que isCiclico e IsSpawner são incompativeis, vc fez esse codigo, para de ser burro!");
		}

		move = false; // se for verdadeiro ele avança para o destino
		
        if (startInDest == true)
        {
            platTransf.position = destinos[destino].position; // posição inicial da plataforma
            
        }

		if (isPlayerDetecting) {
			
		}
		
		
		///StartCoroutine ("mudarPlatDestino");//iniciando o ciclo da movimentação da plataforma

	}
	
	// Update is called once per frame
	void Update ()
	{
		
//		Debug.Log (platTransf);
//Verificando se a plataforma não está no destino para que haja prosseguimento no ciclo do move
		if (!DoesItReturn && destino == destinos.Length - 1) {

		} else {
			if (platTransf.position != destinos [destino].position) {
				move = true;
			} else {
				move = false;
				troca = true;
				if (!isJammerLocked) {
					if (!IsInvoking ("mudarPlatDestino")) {
						Invoke ("mudarPlatDestino", timerTrocaDestino);
					}
				} else if (destino != 0) {
					if (!IsInvoking ("mudarPlatDestino")) {
						Invoke ("mudarPlatDestino", timerTrocaDestino);
					}
				} else {
					spr.color = new Color32 (0, 255, 55, 255);
				}
		
			}
		}

//fazendo com que avançe para o novo destino

		if (isSpawner && direcao < 0) {
			destino = 0;
			platTransf.SetPositionAndRotation (destinos [destino].position, destinos [destino].rotation);
			direcao = 1;
		} else {
			platTransf.position = Vector3.MoveTowards (platTransf.position, destinos [destino].position, velo * Time.deltaTime);
		}

        if (isPlayerDetecting)
        {
            if(Global.morto == true && direcao == 1)
            {
                direcao = -1;
            }
        }
	}

	void mudarPlatDestino ()
	{
		if (destino == destinos.Length - 1 || destino == 0) {
			if (isPlayerDetecting) {
				if (destino == 0) {
					foreach (Transform child in transform) {
						if (child.tag.Equals ("Player")) {
							destino = 1;
							direcao = 1;
						}

					}

				} else if (destino == destinos.Length - 1 ) {

					direcao = -1;
					
					destino = destinos.Length - 2;
					foreach (Transform child in transform) {
						if (child.tag.Equals ("Player")) {
							
							destino = destinos.Length - 1;
							break;
						}

					}

				} else {
					resolveTurning ();
				}
		
			} else {
				resolveTurning ();
			}
		}/* else if (isPlayerDetecting) {
			if (Vector3.Distance(platTransf.position,Global.PlayerAcess.transform.position) > 5) {
				direcao = -1;
			} 
				destino += 1 * direcao;
			}*/
		else {
			destino += 1 * direcao;
		}

		troca = false;

	}

	public void resolveTurning ()
	{
		if (!isCiclico) {
			direcao = (destino == 0) ? 1 : -1;
			destino += 1 * direcao;

		} else {
			destino = (destino == 0) ? 1 : 0;

		}
	}

	public void JammerPlatUpdate(){
		if (destino == 0 && troca == true) {
			mudarPlatDestino ();
			spr.color = new Color32 (104,0,255,255);
		}
	}
	
}