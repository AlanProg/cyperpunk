﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Atravessavel : MonoBehaviour {
	
	private Transform platTransf;
	private Transform playerTransf;
	private BoxCollider2D platCol;
	private Player playerScript;
	private float timer;
	private bool desceu;

	// Use this for initialization
	void Start () {

		platTransf = transform;
		platCol = GetComponent<BoxCollider2D> ();
		desceu = false;
		timer = 0;

	}
	
	// Update is called once per frame
	void Update () {

		playerTransf = (FindObjectOfType(typeof(Player)) as Player).transform;

		playerScript = FindObjectOfType(typeof(Player)) as Player;

		if(desceu && timer >= 1) desceu = false;

		if (desceu)	timer += Time.deltaTime;

//Situações de ativar e desativar colisor da plataforma

		if (playerTransf.position.y - playerTransf.localScale.y/2 > platTransf.position.y + platTransf.localScale.y / 2 && !desceu && 
			playerScript.playerRB.velocity.y <= 0) {
			platCol.enabled = true;
		} else {	platCol.enabled = false;	}

		if (Input.GetKeyDown ("space") && Input.GetAxisRaw ("Vertical") < 0 && platCol.enabled == true && !desceu) {
			timer = 0;
			desceu = true;
			platCol.enabled = false;
		}
	}

}
