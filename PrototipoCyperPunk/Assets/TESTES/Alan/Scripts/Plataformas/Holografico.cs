﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Holografico : MonoBehaviour {

	[Header("Tempo que ficará desativado")]
	public float tempoOff;
	[Header("Tempo que ficará ativado após o toque")]
	public float tempo;
	[Header("Layers que podem desativar instantaneamente ao colidir")]
	public LayerMask podeDestruir;

	private BoxCollider2D platCol;
	private bool desativa, desativaInst;
	private float timer;

	public GameObject platChild;
	// Use this for initialization
	void Start () {
		
		desativa = desativaInst = false;

	}
	
	// Update is called once per frame
	void Update () {
		
		timer += Time.deltaTime;
		
		if (!platChild.activeSelf && !desativa && !desativaInst && timer >= tempoOff) {
			timer = 0;
			platChild.SetActive (true);
		}
		
		
		if (desativa) {
			if (timer >= tempo) {
				desativa = false;
				timer = 0;
				platChild.SendMessage ("disableMe");
			}
		}

		if (desativaInst){
			timer = 0;
			desativa = false;
			desativaInst = false;
			platChild.SendMessage ("disableMe");
		}
			
	}

	void OnCollisionEnter2D(Collision2D col){

		if (col.gameObject.layer == podeDestruir) {
			timer = 0;
			desativaInst = true;
		}
		 if (col.gameObject.layer == 15) {
			timer = 0;
			desativa = true;
		}
	}

	void OnTriggerEnter2D(Collider2D col){
		
		if (podeDestruir == (podeDestruir | (1 << col.gameObject.layer))) {
			timer = 0;
			desativaInst = true;
		}
		if (col.gameObject.layer == 15) {
			timer = 0;
			desativa = true;
		}

	}

}