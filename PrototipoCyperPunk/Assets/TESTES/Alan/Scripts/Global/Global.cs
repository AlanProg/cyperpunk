﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Global : MonoBehaviour {

	public Player playerScript;
	public __GC__ GCScript;
	public PlayerGun playerGunScript;
	public PlayerTime playerTimeScript;
	public GameObject camera;

	public static GameObject cameraa;

	/// <summary>
	/// Variáveis de auxilo para o Rômulo aplicar sons
	/// </summary>

	public static int Tema; // 1 - Tier 1 .  2 - Tier 2 . 3 - Power Ups do Tier 1 . 4 - Power Up do tier 2 . 5 - BossBattle Cerberus . 6 - BossBattle APOLO
	public static int randomValue; // Randomico de 1 a 5 sempre que avançar um nivel
	public static int qual_metade_Cerb; // separado em 4 quartos de 100, a vida tem 4 fases , 100 - 75- 50 - 25
	public static int qual_metade_APOLO; // separado em 4 quartos de 100, a vida tem 4 fases , 100 - 75- 50 - 25
	public static int Especial_Boss_Cerb; // variavel com valor negativo e positivo dependendo de quando é executado o ataque especial
	public static int Especial_Boss_APOLO; // variavel com valor positivo dependendo de quando é executado o ataque especial
	public static int OutroAtq_Boss_Cerb; // variavel com valor positivo dependendo de quando é executado o ataque comum
	public static int OutroAtq_Boss_APOLO; // variavel com valor positivo dependendo de quando é executado o ataque comum

	/// <summary>
	/// Variáveis de auxilo para o Rômulo aplicar sons
	/// </summary>


	[Header("Esconder itens de teste das cenas ao dar play")]
	public bool esconder;

	public static string[] titulo, historia; 

	public static bool esconderTestes;
	public static bool primeiroSum = true;

	public static string[] tuto = new string[4];

	public static bool pause, gameOver;
	public static Rigidbody2D playerRB;
	public static float life;
	public static float Energia; 
	public static bool isKnock;
	public static bool isKnockOut;
	public static bool modeState;
	public static bool recarregando;
	public static bool onScan;
	public static bool onStasi;
	public static bool onDash;
	public static bool dash = false;
	public static bool wallJump = false;
	public static bool doubleJump = false;
	public static bool groundPound = false;
	public static bool stasi = true;
	public static bool jammer = false;
	public static bool granada = false;
	public static string ultimoOlhar;
	public static int tutoAtual;
	public static int saida;
	public static bool onRampa;
	public static bool morto;
	public static float lifeMax = 100;
	public static float energiaMax = 100;

	public static bool damage;
	public static Vector3 lastPositionToLoad;

	public static int[] balasNaCena , municao;
	public static int armaEquipada;
	public static bool granadaNaCena;

	public static __GC__ GCAcess;
	public static Player PlayerAcess;

	public static Vector3 alvo;
	public static float danoFrag;
	public static int quantFrag;
	public static float forceX, forceY; 


	public static bool loadMorte;

	void Start(){
		
		GCAcess = GCScript;
		PlayerAcess = playerScript;

		if (!esconderTestes) {
			esconderTestes = esconder;
		} else {
			esconder = esconderTestes;
		}
			
		pause = gameOver = isKnock = isKnockOut = modeState = damage = false;
		playerRB = playerScript.playerRB;
		life = 100;
		Energia = 100;
		ultimoOlhar = "dir";

		balasNaCena = new int[3]; 
		municao = new int[3];
		playerGunScript.MunicaoInicio (0);
		playerGunScript.MunicaoInicio (2);
		playerGunScript.MunicaoInicio (1);
		armaEquipada = playerGunScript.armaEquipada;

		cameraa = camera;
	
	}

	void update(){
		
	}
}
