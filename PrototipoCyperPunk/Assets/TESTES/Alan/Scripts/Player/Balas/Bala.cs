﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour {

	public float veloBala;
	public float dmgBala;
	public int distanceMax;
	private Rigidbody2D balaRB;
	private Transform cameraT;

	public GameObject deathParticle;

	// Use this for initialization
	void Start () {

		balaRB = GetComponent<Rigidbody2D> ();

		cameraT = GameObject.Find ("RefBala").GetComponent<Transform> ();

		Global.balasNaCena[0] += 1;

		if (Input.GetAxisRaw ("Vertical") > 0) {
			balaRB.velocity = new Vector2 (0, veloBala);
		} else if (Input.GetAxisRaw ("Vertical") < 0) {
			balaRB.velocity = new Vector2 (0, veloBala * -1);
		} else if (Global.ultimoOlhar == "dir" || Input.GetAxisRaw ("Horizontal") > 0) {
			balaRB.velocity = new Vector2 (veloBala, 0);
		} else if (Global.ultimoOlhar == "esq" || Input.GetAxisRaw ("Horizontal") < 0) {
			balaRB.velocity = new Vector2 (veloBala * -1, 0);
		}

	}
	
	// Update is called once per frame
	void Update () {

		if (balaRB.position.x > cameraT.position.x + distanceMax || balaRB.position.x < cameraT.position.x - distanceMax){
			Global.balasNaCena[0] -= 1;
			PlayDeathParticle ();
		}
		if (balaRB.position.y > cameraT.position.y + distanceMax || balaRB.position.y < cameraT.position.y - distanceMax){
			Global.balasNaCena[0] -= 1;
			PlayDeathParticle ();
		}

}

	void OnTriggerEnter2D(Collider2D col){

		switch(col.gameObject.tag){
		case "Enemy":
			col.gameObject.SendMessage("atingidoBala", dmgBala);
			Global.balasNaCena[0] -= 1;
			PlayDeathParticle ();		
			break;
		case "Chao":
			Global.balasNaCena[0] -= 1;
			PlayDeathParticle ();		
			break;
		}
	}

	void PlayDeathParticle(){
		Instantiate (deathParticle, transform.position, transform.rotation);
		Destroy (gameObject);
	}

}