﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GranadaBala : MonoBehaviour {



	public float forcaX;
	public float forcaY;
	public float dmgBala;
	public float tempoExplosao;
	private Rigidbody2D balaRB;
	private float timer;
	private bool iniciaTimer;

	public GameObject deathParticle;
    //public AudioClip b00msound;
    //public float b00mVolume = 1f;

    // Use this for initialization
    void Start () {

		balaRB = GetComponent<Rigidbody2D> ();

		timer = 0;

		Global.granadaNaCena = true;
		
		Global.balasNaCena[2] += 1;

		if(Global.ultimoOlhar == "dir" || Input.GetAxisRaw("Horizontal") > 0)
			balaRB.AddForce(new Vector2(forcaX, forcaY));
		else
			if(Global.ultimoOlhar == "esq" || Input.GetAxisRaw("Horizontal") < 0)
				balaRB.AddForce(new Vector2(forcaX * -1, forcaY));
			else
				if(Global.ultimoOlhar == "cim" || Input.GetAxisRaw("Vertical") > 0)
					balaRB.AddForce(new Vector2(0, forcaY));
				else
					if(Global.ultimoOlhar == "bai" || Input.GetAxisRaw("Vertical") < 0)
						balaRB.AddForce(new Vector2(0, forcaY * -1));
		
	}
	
	// Update is called once per frame
	void Update () {

		if (iniciaTimer)
			timer += Time.deltaTime;

		if (balaRB.velocity.x > 0) {
			balaRB.velocity = new Vector2 (balaRB.velocity.x - 0.2f, balaRB.velocity.y);
		}else if(balaRB.velocity.x < 0) {
			balaRB.velocity = new Vector2 (balaRB.velocity.x + 0.2f, balaRB.velocity.y);
		}
		
	}

	void OnColliderEnter2D(Collision2D col){
		print ("porquecaralhos");
		switch(col.collider.gameObject.tag){
		case "Enemy":
			col.gameObject.SendMessage ("atingidoGranada", dmgBala);
			Global.balasNaCena [2] -= 1;
			Global.granadaNaCena = false;
			PlayDeathParticle ();
			break;
		case "Plataforma":
			if (!iniciaTimer) {
				timer = 0;
				iniciaTimer = true;
				print ("detecta");
			}
			break;
		case "Chao":
			if (!iniciaTimer) {
				timer = 0;
				iniciaTimer = true;
			}
			break;
		case "Obstaculo":
			col.gameObject.SendMessage("atingidoGranada", dmgBala);
			Global.balasNaCena[2] -= 1;
			Global.granadaNaCena = false;
			print ("Explodi Obstaculo");
			PlayDeathParticle ();
			break;
		case "Boss":
			col.gameObject.SendMessage ("atingidoGranada", dmgBala);
			Global.balasNaCena [2] -= 1;
			Global.granadaNaCena = false;
			print ("Explodi Boss");
			PlayDeathParticle ();
			break;
		}
	}
	void OnTriggerStay2D(Collider2D col){

		if (timer >= tempoExplosao) {
			switch (col.gameObject.tag) {
			case "Enemy":
				col.gameObject.SendMessage ("atingidoGranada", dmgBala);
				Global.balasNaCena [2] -= 1;
				Global.granadaNaCena = false;
				PlayDeathParticle ();
				break;
			case "Plataforma":
				col.gameObject.SendMessage ("atingidoGranada", dmgBala);
				Global.balasNaCena [2] -= 1;
				Global.granadaNaCena = false;
				PlayDeathParticle ();
				break;
			case "Chao":
				col.gameObject.SendMessage ("atingidoGranada", dmgBala);
				Global.balasNaCena [2] -= 1;
				Global.granadaNaCena = false;
				PlayDeathParticle ();
				break;
			case "Obstaculo":
				col.gameObject.SendMessage ("atingidoGranada", dmgBala);
				Global.balasNaCena [2] -= 1;
				Global.granadaNaCena = false;
				PlayDeathParticle ();
				break;
			case "Boss":
				col.gameObject.SendMessage ("atingidoGranada", dmgBala);
				Global.balasNaCena [2] -= 1;
				Global.granadaNaCena = false;
				PlayDeathParticle();
				break;
			}
		}
	}


	void PlayDeathParticle(){
		Instantiate (deathParticle, transform.position, transform.rotation);
       // AudioSource.PlayClipAtPoint(b00msound, balaRB.transform.position, b00mVolume);
        Destroy (gameObject);
	}

}
