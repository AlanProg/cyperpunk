﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JammerBala : MonoBehaviour {
	
	public float veloBala;
	public float dmgBala;
	public int distanceMax;
	private Rigidbody2D balaRB;
	private Transform cameraT;
	private float descansoRammer;

	public static float descRammer;
	public GameObject deathParticle;


	// Use this for initialization
	void Start () {

		Global.balasNaCena[1] += 1;
		balaRB = GetComponent<Rigidbody2D> ();

		if (Input.GetAxisRaw ("Vertical") > 0) {
			balaRB.velocity = new Vector2 (0, veloBala);
		} else if (Input.GetAxisRaw ("Vertical") < 0) {
			balaRB.velocity = new Vector2 (0, veloBala * -1);
		} else if (Global.ultimoOlhar == "dir" || Input.GetAxisRaw ("Horizontal") > 0) {
			balaRB.velocity = new Vector2 (veloBala, 0);
		} else if (Global.ultimoOlhar == "esq" || Input.GetAxisRaw ("Horizontal") < 0) {
			balaRB.velocity = new Vector2 (veloBala * -1, 0);
		}
		
	}

	// Update is called once per frame
	void Update () {

		if (descansoRammer != descRammer)
			descansoRammer = descRammer;

		cameraT = GameObject.Find ("RefBala").GetComponent<Transform> ();

		if (balaRB.position.x > cameraT.position.x + distanceMax || balaRB.position.x < cameraT.position.x - distanceMax){
			Global.balasNaCena[1] -= 1;
			Destroy (gameObject);
		}
		if (balaRB.position.y > cameraT.position.y + distanceMax || balaRB.position.y < cameraT.position.y - distanceMax){
			Global.balasNaCena[1] -= 1;
			Destroy (gameObject);
		}

	}

	void OnTriggerEnter2D(Collider2D col){

		switch(col.gameObject.tag){
		case "Enemy":
			col.gameObject.SendMessage ("atingidoJammer", dmgBala);
			Global.balasNaCena [1] -= 1;
			PlayDeathParticle ();
			break;
		case "Plataforma":
			Global.balasNaCena[1] -= 1;
			PlayDeathParticle ();		
			break;
		case "Chao":
			Global.balasNaCena[1] -= 1;
			PlayDeathParticle ();
			break;
		}
	}


	void PlayDeathParticle(){
		Instantiate (deathParticle, transform.position, transform.rotation);
		Destroy (gameObject);
	}
}