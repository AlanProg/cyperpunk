﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine.Utility;
using Cinemachine.Timeline;

public class InverteLado : MonoBehaviour {


	public GameObject[] Cinemachines;

	private Transform playerT;
	private float contador = 0;
	private bool troca = false;
    public float distance = 9;

	// Use this for initialization
	void Start (){


	}
	
	// Update is called once per frame
	void Update () {

		playerT = GameObject.FindGameObjectWithTag ("Player").transform;

		float distanceX;
		distanceX = Mathf.Abs(playerT.position.x - transform.position.x);

		if (contador >= 3)
			troca = false;

		if (troca == true)
			contador += Time.deltaTime;


		if (distanceX >= distance && Cinemachines [0].activeInHierarchy && troca == false) {
			contador = 0;
			troca = true;
			Cinemachines [0].SetActive (false);
			Cinemachines [1].SetActive (true);
		} else if (distanceX >= distance && Cinemachines [1].activeInHierarchy && troca == false) {
			contador = 0;
			troca = true;
			Cinemachines [0].SetActive (true);
			Cinemachines [1].SetActive (false);
		}

	}
}
