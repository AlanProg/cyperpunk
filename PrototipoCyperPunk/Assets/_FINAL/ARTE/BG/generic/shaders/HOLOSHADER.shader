// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "HARBINGER/HOLOSHADER"
{
	Properties
	{
		[NoScaleOffset]_MainTex("_MainTex", 2D) = "white" {}
		_alpha("alpha", Range( 0 , 1)) = 0.5
		_Smoothness("Smoothness", Float) = 0
		[NoScaleOffset]_normal("normal", 2D) = "white" {}
		[NoScaleOffset]_Emissivemap("Emissive map", 2D) = "white" {}
		_emissiveoverlay("emissive overlay", Color) = (0,0.6275859,1,0)
		_emissivecolor("emissive color", Color) = (0,1,0.2156863,0)
		_emissivePOWEEERR("emissive POWEEERR", Range( 0 , 3)) = 1
		[NoScaleOffset]_Noise("Noise", 2D) = "white" {}
		[Toggle]_Noiseanimated("Noise animated", Float) = 0
		_Speed("Speed", Range( -3 , 3)) = 2
		_NoiseTile("Noise Tile", Float) = 1
		[Toggle]_UseVertexColor("Use Vertex Color", Float) = 0
		[Toggle]_useworldposition("use world position", Float) = 0
		_normalPOWEEER("normal POWEEER", Float) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Off
		AlphaToMask On
		CGPROGRAM
		#include "UnityStandardUtils.cginc"
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard alpha:fade keepalpha noshadow 
		struct Input
		{
			float2 uv_texcoord;
			fixed ASEVFace : VFACE;
			float4 vertexColor : COLOR;
			float3 worldPos;
		};

		uniform sampler2D _normal;
		uniform float _normalPOWEEER;
		uniform sampler2D _MainTex;
		uniform float _Noiseanimated;
		uniform float _UseVertexColor;
		uniform float4 _emissiveoverlay;
		uniform float4 _emissivecolor;
		uniform float _emissivePOWEEERR;
		uniform sampler2D _Emissivemap;
		uniform sampler2D _Noise;
		uniform float _Speed;
		uniform float _useworldposition;
		uniform float _NoiseTile;
		uniform float _Smoothness;
		uniform float _alpha;


		float3 HSVToRGB( float3 c )
		{
			float4 K = float4( 1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0 );
			float3 p = abs( frac( c.xxx + K.xyz ) * 6.0 - K.www );
			return c.z * lerp( K.xxx, clamp( p - K.xxx, 0.0, 1.0 ), c.y );
		}


		float3 RGBToHSV(float3 c)
		{
			float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
			float4 p = lerp( float4( c.bg, K.wz ), float4( c.gb, K.xy ), step( c.b, c.g ) );
			float4 q = lerp( float4( p.xyw, c.r ), float4( c.r, p.yzx ), step( p.x, c.r ) );
			float d = q.x - min( q.w, q.y );
			float e = 1.0e-10;
			return float3( abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
		}

		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_normal = i.uv_texcoord;
			float switchResult149 = (((i.ASEVFace>0)?(_normalPOWEEER):(( _normalPOWEEER * -1.0 ))));
			o.Normal = UnpackScaleNormal( tex2D( _normal, uv_normal ) ,switchResult149 );
			float2 uv_MainTex = i.uv_texcoord;
			float4 tex2DNode18 = tex2D( _MainTex, uv_MainTex );
			o.Albedo = tex2DNode18.rgb;
			float3 hsvTorgb113 = RGBToHSV( _emissivecolor.rgb );
			float3 hsvTorgb115 = HSVToRGB( float3(hsvTorgb113.x,hsvTorgb113.y,_emissivePOWEEERR) );
			float2 uv_Emissivemap = i.uv_texcoord;
			float4 temp_output_118_0 = ( lerp(_emissiveoverlay,i.vertexColor,_UseVertexColor) + ( float4( hsvTorgb115 , 0.0 ) * tex2D( _Emissivemap, uv_Emissivemap ) ) );
			float2 temp_cast_5 = (_Speed).xx;
			float2 uv_TexCoord123 = i.uv_texcoord * float2( 1,1 ) + float2( 0,0 );
			float3 ase_worldPos = i.worldPos;
			float2 panner126 = ( lerp(float3( uv_TexCoord123 ,  0.0 ),ase_worldPos,_useworldposition).xy + _Time.y * temp_cast_5);
			float mulTime131 = _Time.y * 0.0;
			float2 temp_cast_8 = (frac( ( ( tex2D( _Noise, panner126 ).r * _NoiseTile ) + mulTime131 ) )).xx;
			float simplePerlin2D134 = snoise( temp_cast_8 );
			o.Emission = abs( lerp(temp_output_118_0,( temp_output_118_0 * simplePerlin2D134 ),_Noiseanimated) ).rgb;
			o.Smoothness = _Smoothness;
			o.Alpha = ( ( tex2DNode18.a * _alpha ) + ( lerp(temp_output_118_0,( temp_output_118_0 * simplePerlin2D134 ),_Noiseanimated) * _alpha ) ).r;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14101
60;96;1313;744;1549.866;1378.01;1.6;True;False
Node;AmplifyShaderEditor.TextureCoordinatesNode;123;-1736.287,-98.648;Float;True;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WorldPosInputsNode;138;-1686.072,-236.4494;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ToggleSwitchNode;139;-1481.297,-95.25702;Float;False;Property;_useworldposition;use world position;13;0;Create;0;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleTimeNode;125;-1490.444,461.2784;Float;False;1;0;FLOAT;1.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;124;-1657.146,177.7083;Float;False;Property;_Speed;Speed;10;0;Create;2;-3;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;126;-1316.412,126.2313;Float;True;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1.0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.CommentaryNode;119;-1376.196,-999.5854;Float;False;1178.09;813.3809;emissive;9;111;113;115;117;118;30;112;136;137;;1,1,1,1;0;0
Node;AmplifyShaderEditor.ColorNode;111;-1326.196,-949.5855;Float;False;Property;_emissivecolor;emissive color;6;0;Create;0,1,0.2156863,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;127;-697.0709,856.389;Float;False;Constant;_Float4;Float 4;5;0;Create;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;129;-713.7739,94.66848;Float;True;Property;_Noise;Noise;8;1;[NoScaleOffset];Create;Assets/TOOLS/AmplifyShaderEditor/Examples/Assets/Textures/Sand/Sand_height.tga;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;128;-605.1859,567.6252;Float;False;Property;_NoiseTile;Noise Tile;11;0;Create;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;131;-532.014,771.5629;Float;False;1;0;FLOAT;1.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;112;-1236.272,-384.8904;Float;False;Property;_emissivePOWEEERR;emissive POWEEERR;7;0;Create;1;0;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.RGBToHSVNode;113;-1129.969,-691.3031;Float;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;130;-527.186,415.5255;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;30;-899.2604,-392.6579;Float;True;Property;_Emissivemap;Emissive map;4;1;[NoScaleOffset];Create;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.VertexColorNode;135;-677.4041,-1183.696;Float;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;132;-374.8849,410.8524;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.HSVToRGBNode;115;-873.8676,-666.603;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ColorNode;137;-865.041,-919.452;Float;False;Property;_emissiveoverlay;emissive overlay;5;0;Create;0,0.6275859,1,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ToggleSwitchNode;136;-534.0806,-837.3625;Float;False;Property;_UseVertexColor;Use Vertex Color;12;0;Create;0;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;117;-598.2348,-615.5948;Float;False;2;2;0;FLOAT3;0.0,0,0,0;False;1;COLOR;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.FractNode;133;-233.6744,441.0945;Float;False;1;0;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;134;-60.42889,383.1798;Float;False;Simplex2D;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;118;-352.1068,-651.1739;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0.0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;105;399.7625,57.32245;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0.0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;144;269.23,-376.3837;Float;False;Property;_normalPOWEEER;normal POWEEER;14;0;Create;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;68;495.9229,-235.2943;Float;False;Property;_Noiseanimated;Noise animated;9;0;Create;0;2;0;COLOR;0.0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;18;-142.965,-941.6279;Float;True;Property;_MainTex;_MainTex;0;1;[NoScaleOffset];Create;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;121;613.6862,-66.6591;Float;False;Property;_alpha;alpha;1;0;Create;0.5;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;150;502.8881,-344.7379;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;-1.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SwitchByFaceNode;149;692.8881,-375.7379;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;120;1029.075,-54.87544;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;147;966.3521,123.4341;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;3;196.2698,-565.6212;Float;True;Property;_normal;normal;3;1;[NoScaleOffset];Create;None;True;0;True;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.UnpackScaleNormalNode;140;868.5903,-452.5903;Float;False;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;1.0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;35;1072.063,-527.1215;Float;False;Property;_Smoothness;Smoothness;2;0;Create;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.AbsOpNode;148;941.6748,-180.3385;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;122;1132.398,105.1378;Float;False;2;2;0;FLOAT;0.0;False;1;COLOR;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;2;1364.165,-202.8348;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;HARBINGER/HOLOSHADER;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;Off;0;0;False;0;0;Transparent;0.5;True;False;0;False;Transparent;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;False;2;SrcAlpha;OneMinusSrcAlpha;2;SrcAlpha;OneMinusSrcAlpha;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;True;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;139;0;123;0
WireConnection;139;1;138;0
WireConnection;126;0;139;0
WireConnection;126;2;124;0
WireConnection;126;1;125;0
WireConnection;129;1;126;0
WireConnection;131;0;127;0
WireConnection;113;0;111;0
WireConnection;130;0;129;1
WireConnection;130;1;128;0
WireConnection;132;0;130;0
WireConnection;132;1;131;0
WireConnection;115;0;113;1
WireConnection;115;1;113;2
WireConnection;115;2;112;0
WireConnection;136;0;137;0
WireConnection;136;1;135;0
WireConnection;117;0;115;0
WireConnection;117;1;30;0
WireConnection;133;0;132;0
WireConnection;134;0;133;0
WireConnection;118;0;136;0
WireConnection;118;1;117;0
WireConnection;105;0;118;0
WireConnection;105;1;134;0
WireConnection;68;0;118;0
WireConnection;68;1;105;0
WireConnection;150;0;144;0
WireConnection;149;0;144;0
WireConnection;149;1;150;0
WireConnection;120;0;18;4
WireConnection;120;1;121;0
WireConnection;147;0;68;0
WireConnection;147;1;121;0
WireConnection;140;0;3;0
WireConnection;140;1;149;0
WireConnection;148;0;68;0
WireConnection;122;0;120;0
WireConnection;122;1;147;0
WireConnection;2;0;18;0
WireConnection;2;1;140;0
WireConnection;2;2;148;0
WireConnection;2;4;35;0
WireConnection;2;9;122;0
ASEEND*/
//CHKSM=B6CA799CA5D247A0634CCEE0B14AB1E7A924694C