// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "HARBINGER/FILL_standard"
{
	Properties
	{
		_TILING("TILING", Float) = 1
		[NoScaleOffset]_basecolor("base color", 2D) = "white" {}
		[NoScaleOffset]_normal("normal", 2D) = "white" {}
		_NormalPOWEERRRR("Normal POWEERRRR", Float) = 0
		[NoScaleOffset]_roughness("roughness", 2D) = "white" {}
		_metallic("metallic", Float) = 0
		_roughnessPOWEEEER("roughness POWEEEER", Float) = 0
		[NoScaleOffset]_EmissiveMap("Emissive Map", 2D) = "white" {}
		_emissivecolor("emissive color", Color) = (0,1,0.2156863,0)
		_EmissivePOWEEEEER("Emissive POWEEEEER", Range( 0 , 3)) = 1
		_OverlayEmissive("Overlay Emissive", Color) = (0,0,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityStandardUtils.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
			float4 vertexColor : COLOR;
		};

		uniform sampler2D _normal;
		uniform float _TILING;
		uniform float _NormalPOWEERRRR;
		uniform sampler2D _basecolor;
		uniform float4 _OverlayEmissive;
		uniform float4 _emissivecolor;
		uniform float _EmissivePOWEEEEER;
		uniform sampler2D _EmissiveMap;
		uniform float _metallic;
		uniform sampler2D _roughness;
		uniform float _roughnessPOWEEEER;


		float3 HSVToRGB( float3 c )
		{
			float4 K = float4( 1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0 );
			float3 p = abs( frac( c.xxx + K.xyz ) * 6.0 - K.www );
			return c.z * lerp( K.xxx, clamp( p - K.xxx, 0.0, 1.0 ), c.y );
		}


		float3 RGBToHSV(float3 c)
		{
			float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
			float4 p = lerp( float4( c.bg, K.wz ), float4( c.gb, K.xy ), step( c.b, c.g ) );
			float4 q = lerp( float4( p.xyw, c.r ), float4( c.r, p.yzx ), step( p.x, c.r ) );
			float d = q.x - min( q.w, q.y );
			float e = 1.0e-10;
			return float3( abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 temp_cast_0 = (_TILING).xx;
			float2 uv_TexCoord23 = i.uv_texcoord * temp_cast_0 + float2( 0,0 );
			o.Normal = UnpackScaleNormal( tex2D( _normal, uv_TexCoord23 ) ,_NormalPOWEERRRR );
			o.Albedo = ( i.vertexColor * tex2D( _basecolor, uv_TexCoord23 ) ).rgb;
			float3 hsvTorgb9 = RGBToHSV( _emissivecolor.rgb );
			float3 hsvTorgb12 = HSVToRGB( float3(hsvTorgb9.x,hsvTorgb9.y,_EmissivePOWEEEEER) );
			o.Emission = ( _OverlayEmissive + ( float4( hsvTorgb12 , 0.0 ) * tex2D( _EmissiveMap, uv_TexCoord23 ) ) ).rgb;
			o.Metallic = _metallic;
			o.Smoothness = ( tex2D( _roughness, uv_TexCoord23 ) * _roughnessPOWEEEER ).r;
			float4 temp_output_1_0 = i.vertexColor;
			o.Occlusion = temp_output_1_0.r;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14101
224;236;1388;701;1807.435;66.25146;2.275569;True;True
Node;AmplifyShaderEditor.CommentaryNode;15;-1359.762,664.1432;Float;False;1178.09;813.3812;emissive;8;7;8;9;10;11;12;13;14;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;22;-2017.106,236.5848;Float;False;Property;_TILING;TILING;0;0;Create;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;8;-1309.762,714.1432;Float;False;Property;_emissivecolor;emissive color;8;0;Create;0,1,0.2156863,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;23;-1760.214,211.5116;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;10;-1107.833,1140.125;Float;False;Property;_EmissivePOWEEEEER;Emissive POWEEEEER;9;0;Create;1;0;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.RGBToHSVNode;9;-1113.535,972.4258;Float;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SamplerNode;11;-1176.747,1247.524;Float;True;Property;_EmissiveMap;Emissive Map;7;1;[NoScaleOffset];Create;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;18;-854.0112,254.3111;Float;False;661.6024;398.9004;Normal;3;4;17;16;;1,1,1,1;0;0
Node;AmplifyShaderEditor.HSVToRGBNode;12;-857.4332,997.1259;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;16;-723.9717,538.2114;Float;False;Property;_NormalPOWEERRRR;Normal POWEERRRR;3;0;Create;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;14;-904.6266,818.0691;Float;False;Property;_OverlayEmissive;Overlay Emissive;10;0;Create;0,0,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;3;-643.1972,-305.4474;Float;True;Property;_basecolor;base color;1;1;[NoScaleOffset];Create;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.VertexColorNode;1;-545.9735,-506.5877;Float;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;5;-1170.877,-121.1116;Float;True;Property;_roughness;roughness;4;1;[NoScaleOffset];Create;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;13;-581.8004,1048.134;Float;False;2;2;0;FLOAT3;0.0,0,0,0;False;1;COLOR;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;20;-1144.536,149.4989;Float;False;Property;_roughnessPOWEEEER;roughness POWEEEER;6;0;Create;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;4;-804.0112,304.3111;Float;True;Property;_normal;normal;2;1;[NoScaleOffset];Create;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;7;-335.6722,1012.555;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0.0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.UnpackScaleNormalNode;17;-456.4096,361.8739;Float;False;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;1.0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;21;75.56616,515.5578;Float;False;Property;_metallic;metallic;5;0;Create;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;2;-293.3084,-209.7642;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;19;-802.2548,56.56402;Float;False;2;2;0;COLOR;0.0;False;1;FLOAT;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;325.196,424.2791;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;HARBINGER/FILL_standard;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;Opaque;0.5;True;True;0;False;Opaque;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;23;0;22;0
WireConnection;9;0;8;0
WireConnection;11;1;23;0
WireConnection;12;0;9;1
WireConnection;12;1;9;2
WireConnection;12;2;10;0
WireConnection;3;1;23;0
WireConnection;5;1;23;0
WireConnection;13;0;12;0
WireConnection;13;1;11;0
WireConnection;4;1;23;0
WireConnection;7;0;14;0
WireConnection;7;1;13;0
WireConnection;17;0;4;0
WireConnection;17;1;16;0
WireConnection;2;0;1;0
WireConnection;2;1;3;0
WireConnection;19;0;5;0
WireConnection;19;1;20;0
WireConnection;0;0;2;0
WireConnection;0;1;17;0
WireConnection;0;2;7;0
WireConnection;0;3;21;0
WireConnection;0;4;19;0
WireConnection;0;5;1;0
ASEEND*/
//CHKSM=BB430C9E684F6FBFB19AD465299572E831B09607