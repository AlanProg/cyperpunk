// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "HARBINGER/Prop Shader"
{
	Properties
	{
		_alpha("alpha", Float) = 1
		[NoScaleOffset]_MainTex("_MainTex", 2D) = "white" {}
		_OverlayColor("Overlay Color", Color) = (1,1,1,0)
		[NoScaleOffset]_normal("normal", 2D) = "white" {}
		_normalpower("normal power", Float) = 1
		_Smoothness("Smoothness", Float) = 0
		[NoScaleOffset]_Emissivemap("Emissive map", 2D) = "white" {}
		[Toggle]_UseVertexColor("Use Vertex Color", Float) = 1
		[HDR]_emissivecolor("emissive color", Color) = (0,1,0.2156863,0)
		_emissivePOWEEERR("emissive POWEEERR", Range( 0 , 3)) = 1
		[HDR]_emissiveoverlay("emissive overlay", Color) = (0,0,0,0)
		[Toggle]_Noiseanimated("Noise animated", Float) = 0
		[Toggle]_Vertical("Vertical", Float) = 0
		[Toggle]_UseTexCoord("Use Tex Coord", Float) = 0
		[Toggle]_TexcoordVertical("Tex coord Vertical", Float) = 0
		_NoiseContrast("Noise Contrast", Range( 0 , 1)) = 0.5
		_noisescale("noise scale", Float) = 0
		_Wavespeed("Wave speed", Float) = -0.1
		_WaveScale("Wave Scale", Float) = 0.01
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Off
		Blend SrcAlpha OneMinusSrcAlpha , SrcAlpha OneMinusSrcAlpha
		AlphaToMask On
		CGINCLUDE
		#include "UnityStandardUtils.cginc"
		#include "UnityShaderVariables.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		struct Input
		{
			float2 uv_texcoord;
			fixed ASEVFace : VFACE;
			float4 vertexColor : COLOR;
			float3 worldPos;
		};

		uniform sampler2D _normal;
		uniform float _normalpower;
		uniform float4 _OverlayColor;
		uniform sampler2D _MainTex;
		uniform float _Noiseanimated;
		uniform float4 _emissiveoverlay;
		uniform float _UseVertexColor;
		uniform float4 _emissivecolor;
		uniform float _emissivePOWEEERR;
		uniform sampler2D _Emissivemap;
		uniform float _UseTexCoord;
		uniform float _Vertical;
		uniform float _TexcoordVertical;
		uniform float _WaveScale;
		uniform float _Wavespeed;
		uniform float _noisescale;
		uniform float _NoiseContrast;
		uniform float _Smoothness;
		uniform float _alpha;


		float3 HSVToRGB( float3 c )
		{
			float4 K = float4( 1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0 );
			float3 p = abs( frac( c.xxx + K.xyz ) * 6.0 - K.www );
			return c.z * lerp( K.xxx, clamp( p - K.xxx, 0.0, 1.0 ), c.y );
		}


		float3 RGBToHSV(float3 c)
		{
			float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
			float4 p = lerp( float4( c.bg, K.wz ), float4( c.gb, K.xy ), step( c.b, c.g ) );
			float4 q = lerp( float4( p.xyw, c.r ), float4( c.r, p.yzx ), step( p.x, c.r ) );
			float d = q.x - min( q.w, q.y );
			float e = 1.0e-10;
			return float3( abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
		}

		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_normal = i.uv_texcoord;
			float4 tex2DNode3 = tex2D( _normal, uv_normal );
			float switchResult187 = (((i.ASEVFace>0)?(_normalpower):(( _normalpower * -1.0 ))));
			o.Normal = UnpackScaleNormal( tex2DNode3 ,switchResult187 );
			float2 uv_MainTex = i.uv_texcoord;
			float4 tex2DNode18 = tex2D( _MainTex, uv_MainTex );
			o.Albedo = ( _OverlayColor * tex2DNode18 ).rgb;
			float3 hsvTorgb113 = RGBToHSV( lerp(_emissivecolor,i.vertexColor,_UseVertexColor).rgb );
			float3 hsvTorgb115 = HSVToRGB( float3(hsvTorgb113.x,hsvTorgb113.y,( i.vertexColor.a * _emissivePOWEEERR )) );
			float2 uv_Emissivemap = i.uv_texcoord;
			float4 temp_output_118_0 = ( _emissiveoverlay + ( float4( hsvTorgb115 , 0.0 ) * tex2D( _Emissivemap, uv_Emissivemap ) ) );
			float3 ase_worldPos = i.worldPos;
			float2 uv_TexCoord195 = i.uv_texcoord * float2( 1,1 ) + float2( 0,0 );
			float mulTime83 = _Time.y * _Wavespeed;
			float simplePerlin2D69 = snoise( ( ase_worldPos * _noisescale ).xy );
			o.Emission = lerp(temp_output_118_0,( temp_output_118_0 * ( frac( ( ( lerp(lerp(ase_worldPos.x,ase_worldPos.y,_Vertical),lerp(uv_TexCoord195.x,uv_TexCoord195.y,_TexcoordVertical),_UseTexCoord) * _WaveScale ) + mulTime83 ) ) - (simplePerlin2D69*_NoiseContrast + _NoiseContrast) ) ),_Noiseanimated).rgb;
			o.Smoothness = _Smoothness;
			o.Occlusion = tex2DNode3.b;
			o.Alpha = ( tex2DNode18.a * _alpha );
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Standard keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			AlphaToMask Off
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				float4 tSpace0 : TEXCOORD3;
				float4 tSpace1 : TEXCOORD4;
				float4 tSpace2 : TEXCOORD5;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				fixed3 worldNormal = UnityObjectToWorldNormal( v.normal );
				fixed3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				fixed tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				fixed3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			fixed4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = IN.worldPos;
				fixed3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14101
230;195;1350;654;2932.541;1221.231;3.983357;True;False
Node;AmplifyShaderEditor.CommentaryNode;104;-585.2909,176.1544;Float;False;1107.538;1072.417;wave;11;86;89;91;92;83;85;82;100;192;197;198;;1,1,1,1;0;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;195;-776.3022,644.9193;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WorldPosInputsNode;91;-725.6197,827.0099;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ToggleSwitchNode;197;-558.1404,652.0408;Float;False;Property;_TexcoordVertical;Tex coord Vertical;15;0;Create;0;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;198;-525.2033,841.329;Float;False;Property;_Vertical;Vertical;13;0;Create;0;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.VertexColorNode;152;-1582.722,-516.3517;Float;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;119;-1376.196,-999.5854;Float;False;1178.09;813.3809;emissive;9;113;115;116;117;118;30;112;153;188;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;103;-1789.464,15.29879;Float;False;1122.37;606.8326;noise;6;99;102;101;78;69;77;;1,1,1,1;0;0
Node;AmplifyShaderEditor.ColorNode;111;-1614.196,-681.5855;Float;False;Property;_emissivecolor;emissive color;9;1;[HDR];Create;0,1,0.2156863,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ToggleSwitchNode;192;-328.3481,707.0848;Float;False;Property;_UseTexCoord;Use Tex Coord;14;0;Create;0;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;86;-513.3248,990.5712;Float;False;Property;_Wavespeed;Wave speed;18;0;Create;-0.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldPosInputsNode;99;-1739.464,83.95699;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;112;-1326.272,-324.8904;Float;False;Property;_emissivePOWEEERR;emissive POWEEERR;10;0;Create;1;0;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;89;-424.291,642.1273;Float;False;Property;_WaveScale;Wave Scale;19;0;Create;0.01;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;153;-1364.642,-681.7976;Float;False;Property;_UseVertexColor;Use Vertex Color;8;0;Create;1;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;102;-1673.787,353.0872;Float;False;Property;_noisescale;noise scale;17;0;Create;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;188;-1107.533,-456.7775;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;92;-201.2908,477.1273;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;83;-297.1385,912.2529;Float;False;1;0;FLOAT;1.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;101;-1413.885,94.36675;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0.0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RGBToHSVNode;113;-1129.969,-691.3031;Float;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;78;-1257.081,364.1314;Float;False;Property;_NoiseContrast;Noise Contrast;16;0;Create;0.5;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;85;32.67523,909.5713;Float;True;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;69;-1194.816,97.32275;Float;False;Simplex2D;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.HSVToRGBNode;115;-873.8676,-666.603;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SamplerNode;30;-899.2604,-392.6579;Float;True;Property;_Emissivemap;Emissive map;7;1;[NoScaleOffset];Create;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;117;-598.2348,-615.5948;Float;False;2;2;0;FLOAT3;0.0,0,0,0;False;1;COLOR;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;77;-937.0938,65.29879;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;1.0;False;2;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;82;102.2506,598.6219;Float;True;1;0;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;116;-921.0609,-845.6597;Float;False;Property;_emissiveoverlay;emissive overlay;11;1;[HDR];Create;0,0,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;137;959.4644,-106.608;Float;False;Property;_normalpower;normal power;5;0;Create;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;100;287.2469,470.0077;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;186;1168.884,-17.76186;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;-1.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;118;-352.1068,-651.1739;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0.0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;105;399.7625,57.32245;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0.0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SwitchByFaceNode;187;1346.554,-102.9463;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;121;934.5329,142.839;Float;False;Property;_alpha;alpha;1;0;Create;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;3;872.8261,486.5422;Float;True;Property;_normal;normal;4;1;[NoScaleOffset];Create;None;True;0;True;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;18;676.4427,-784.5527;Float;True;Property;_MainTex;_MainTex;2;1;[NoScaleOffset];Create;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;189;1112.103,-843.671;Float;False;Property;_OverlayColor;Overlay Color;3;0;Create;1,1,1,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;190;1496.768,-687.9449;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0.0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ToggleSwitchNode;68;557.4531,-525.3679;Float;False;Property;_Noiseanimated;Noise animated;12;0;Create;0;2;0;COLOR;0.0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;120;1136.096,126.2755;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.UnpackScaleNormalNode;136;1526.64,-99.70914;Float;False;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;1.0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;35;1072.063,-527.1215;Float;False;Property;_Smoothness;Smoothness;6;0;Create;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;2;1988.558,-207.8925;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;HARBINGER/Prop Shader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;Off;0;0;False;0;0;Custom;0.5;True;True;0;True;Transparent;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;2;SrcAlpha;OneMinusSrcAlpha;2;SrcAlpha;OneMinusSrcAlpha;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;0;0;True;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;197;0;195;1
WireConnection;197;1;195;2
WireConnection;198;0;91;1
WireConnection;198;1;91;2
WireConnection;192;0;198;0
WireConnection;192;1;197;0
WireConnection;153;0;111;0
WireConnection;153;1;152;0
WireConnection;188;0;152;4
WireConnection;188;1;112;0
WireConnection;92;0;192;0
WireConnection;92;1;89;0
WireConnection;83;0;86;0
WireConnection;101;0;99;0
WireConnection;101;1;102;0
WireConnection;113;0;153;0
WireConnection;85;0;92;0
WireConnection;85;1;83;0
WireConnection;69;0;101;0
WireConnection;115;0;113;1
WireConnection;115;1;113;2
WireConnection;115;2;188;0
WireConnection;117;0;115;0
WireConnection;117;1;30;0
WireConnection;77;0;69;0
WireConnection;77;1;78;0
WireConnection;77;2;78;0
WireConnection;82;0;85;0
WireConnection;100;0;82;0
WireConnection;100;1;77;0
WireConnection;186;0;137;0
WireConnection;118;0;116;0
WireConnection;118;1;117;0
WireConnection;105;0;118;0
WireConnection;105;1;100;0
WireConnection;187;0;137;0
WireConnection;187;1;186;0
WireConnection;190;0;189;0
WireConnection;190;1;18;0
WireConnection;68;0;118;0
WireConnection;68;1;105;0
WireConnection;120;0;18;4
WireConnection;120;1;121;0
WireConnection;136;0;3;0
WireConnection;136;1;187;0
WireConnection;2;0;190;0
WireConnection;2;1;136;0
WireConnection;2;2;68;0
WireConnection;2;4;35;0
WireConnection;2;5;3;3
WireConnection;2;9;120;0
ASEEND*/
//CHKSM=2D754B6DF4DF7E95CE6870EDC2EFF9AE583C6F0D