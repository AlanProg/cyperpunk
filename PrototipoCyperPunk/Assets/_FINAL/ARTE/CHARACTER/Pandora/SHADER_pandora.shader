// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "HARBINGER/Pandora"
{
	Properties
	{
		[NoScaleOffset]_MainTex("_MainTex", 2D) = "white" {}
		_Overlaycolor("Overlay color", Color) = (1,1,1,0)
		_Alpha("Alpha", Range( 0 , 1)) = 0
		[NoScaleOffset]_EmissiveMap("Emissive Map", 2D) = "white" {}
		_EmissiveColor("Emissive Color", Color) = (0,1,0.2156863,0)
		_emissivepower("emissive power", Range( 0 , 3)) = 1
		_OverlayEmissive("Overlay Emissive", Color) = (0,0,0,0)
		[NoScaleOffset]_NormalMap("Normal Map", 2D) = "white" {}
		_normpower("norm power", Float) = 0
		[Toggle]_useVertexEmission("use Vertex Emission", Float) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Off
		CGPROGRAM
		#include "UnityStandardUtils.cginc"
		#pragma target 3.0
		#pragma surface surf Standard alpha:fade keepalpha noshadow 
		struct Input
		{
			float2 uv_texcoord;
			fixed ASEVFace : VFACE;
			float4 vertexColor : COLOR;
		};

		uniform sampler2D _NormalMap;
		uniform float _normpower;
		uniform float4 _Overlaycolor;
		uniform sampler2D _MainTex;
		uniform float4 _OverlayEmissive;
		uniform float _useVertexEmission;
		uniform float4 _EmissiveColor;
		uniform float _emissivepower;
		uniform sampler2D _EmissiveMap;
		uniform float _Alpha;


		float3 HSVToRGB( float3 c )
		{
			float4 K = float4( 1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0 );
			float3 p = abs( frac( c.xxx + K.xyz ) * 6.0 - K.www );
			return c.z * lerp( K.xxx, clamp( p - K.xxx, 0.0, 1.0 ), c.y );
		}


		float3 RGBToHSV(float3 c)
		{
			float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
			float4 p = lerp( float4( c.bg, K.wz ), float4( c.gb, K.xy ), step( c.b, c.g ) );
			float4 q = lerp( float4( p.xyw, c.r ), float4( c.r, p.yzx ), step( p.x, c.r ) );
			float d = q.x - min( q.w, q.y );
			float e = 1.0e-10;
			return float3( abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_NormalMap = i.uv_texcoord;
			float switchResult130 = (((i.ASEVFace>0)?(_normpower):(( _normpower * -1.0 ))));
			o.Normal = UnpackScaleNormal( tex2D( _NormalMap, uv_NormalMap ) ,switchResult130 );
			float2 uv_MainTex = i.uv_texcoord;
			float4 tex2DNode1 = tex2D( _MainTex, uv_MainTex );
			o.Albedo = ( _Overlaycolor * tex2DNode1 ).rgb;
			float3 hsvTorgb8 = RGBToHSV( lerp(_EmissiveColor,i.vertexColor,_useVertexEmission).rgb );
			float3 hsvTorgb10 = HSVToRGB( float3(hsvTorgb8.x,hsvTorgb8.y,_emissivepower) );
			float2 uv_EmissiveMap = i.uv_texcoord;
			o.Emission = ( _OverlayEmissive + ( float4( hsvTorgb10 , 0.0 ) * tex2D( _EmissiveMap, uv_EmissiveMap ) ) ).rgb;
			o.Alpha = ( tex2DNode1.a * _Alpha );
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14101
60;96;1313;744;1775.303;465.6124;1;True;False
Node;AmplifyShaderEditor.ColorNode;4;-1379.515,-338.9435;Float;False;Property;_EmissiveColor;Emissive Color;4;0;Create;0,1,0.2156863,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.VertexColorNode;132;-1303.964,-50.77942;Float;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ToggleSwitchNode;133;-1113.641,-78.44586;Float;False;Property;_useVertexEmission;use Vertex Emission;9;0;Create;0;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;21;262.2626,-682.9623;Float;False;Property;_normpower;norm power;8;0;Create;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RGBToHSVNode;8;-863.2883,-86.66094;Float;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;9;-857.5862,81.039;Float;False;Property;_emissivepower;emissive power;5;0;Create;1;0;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;131;449.4363,-558.769;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;-1.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.HSVToRGBNode;10;-607.1861,-61.96089;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SamplerNode;2;-926.5004,188.4382;Float;True;Property;_EmissiveMap;Emissive Map;3;1;[NoScaleOffset];Create;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;3;-331.5533,-10.95272;Float;False;2;2;0;FLOAT3;0.0,0,0,0;False;1;COLOR;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;7;341.817,-341.6725;Float;True;Property;_NormalMap;Normal Map;7;1;[NoScaleOffset];Create;None;True;0;True;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;2.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;1;-268.9034,-517.0818;Float;True;Property;_MainTex;_MainTex;0;1;[NoScaleOffset];Create;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;18;-132.3246,-730.6917;Float;False;Property;_Overlaycolor;Overlay color;1;0;Create;1,1,1,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SwitchByFaceNode;130;590.4363,-688.769;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;15;-147.9948,219.9352;Float;False;Property;_Alpha;Alpha;2;0;Create;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;16;-654.3795,-241.0177;Float;False;Property;_OverlayEmissive;Overlay Emissive;6;0;Create;0,0,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;14;90.78485,123.6428;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.UnpackScaleNormalNode;55;797.8255,-517.2998;Float;False;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;1.0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;19;208.0181,-491.4137;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0.0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;17;-85.42494,-46.53147;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0.0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1206.912,-269.9558;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;HARBINGER/Pandora;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;Off;0;0;False;0;0;Transparent;0.5;True;False;0;False;Transparent;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;False;2;SrcAlpha;OneMinusSrcAlpha;0;Zero;Zero;OFF;OFF;0;False;2.58;0,0,0,0;VertexOffset;True;False;Cylindrical;True;Relative;0;;-1;-1;-1;-1;0;0;0;False;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;133;0;4;0
WireConnection;133;1;132;0
WireConnection;8;0;133;0
WireConnection;131;0;21;0
WireConnection;10;0;8;1
WireConnection;10;1;8;2
WireConnection;10;2;9;0
WireConnection;3;0;10;0
WireConnection;3;1;2;0
WireConnection;130;0;21;0
WireConnection;130;1;131;0
WireConnection;14;0;1;4
WireConnection;14;1;15;0
WireConnection;55;0;7;0
WireConnection;55;1;130;0
WireConnection;19;0;18;0
WireConnection;19;1;1;0
WireConnection;17;0;16;0
WireConnection;17;1;3;0
WireConnection;0;0;19;0
WireConnection;0;1;55;0
WireConnection;0;2;17;0
WireConnection;0;9;14;0
ASEEND*/
//CHKSM=F76714FE7DCD17FA0A5BB0C6A23883A97F47015C