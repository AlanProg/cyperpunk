﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class play_fx : StateMachineBehaviour
{
    [Header("objeto de spawn")]
    public bool follow = false;
    public string nomedoobjeto;
    [Header("VFX")]
    public bool vfxOn = true;
    public GameObject particles;
    private GameObject @object;
    public bool includeChildren = true;
    [Header("SFX")]
    public GameObject audio;
    public bool followaudio = false;
    public bool usesfx = true;

    [Header("Som Inicial")]
    public bool startOn = false;
    public AudioClip[] startClip = new AudioClip[1];
    [Range(0, 1)]
    public float startClipVol = 1;
    public float start_delay = 0;
    public bool start_fade_On = false;
    public float start_fade = 1;

    [Header("Som secundário ou loop")]
    public bool loopOn = false;
    public AudioClip[] loopClip = new AudioClip[1];
    [Range(0, 1)]
    public float loopClipVol = 1;
    public float loop_delay = 0;
    public bool loop_fade_On = true;
    public float loop_fade = 1;

    [Header("Som final")]
    public bool endOn = false;
    public AudioClip[] endClip = new AudioClip[1];
    [Range(0, 1)]
    public float endClipVol = 1;
    public float end_delay = 0;
    public bool end_fade_On = false;
    public float end_fade = 1;
    public float stopENDafter = 5;
    
    











    private Transform particlesTransform;       // Reference to the instantiated prefab's transform.
    private ParticleSystem particleSystem;      // Reference to the instantiated prefab's particle system.
    private Transform audioTransform;
    private AudioSource[] audioSystem;

    private float destroyertimer = 10;
    private float timer = 0;
    private Vector3 @scale;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
		
        @object = GameObject.Find(nomedoobjeto);


        //vfx//////////////////////////////////////////////////////////

        if (vfxOn == true)
        {
            if (particleSystem != null) // If the particle system already exists then exit the function.
            {
				
                if (@object.transform.localScale.x < 0)
                {
                    @scale = new Vector3(-1, particleSystem.transform.localScale.y);
                    particleSystem.transform.SetPositionAndRotation(@object.transform.position, @object.transform.rotation);
                    particleSystem.transform.localScale = new Vector3(-1, 1, 1);
                }
                else
                {
                    particleSystem.transform.SetPositionAndRotation(@object.transform.position, @object.transform.rotation);
                    particleSystem.transform.localScale = new Vector3(1, 1, 1);
                }
                particleSystem.Play(includeChildren);
                
            }
            else
            {
                GameObject particlesInstance = Instantiate(particles, @object.transform.position, @object.transform.rotation);
                particlesTransform = particlesInstance.transform;
                particleSystem = particlesInstance.GetComponent<ParticleSystem>();
                particleSystem.transform.SetPositionAndRotation(@object.transform.position, @object.transform.rotation);
                particleSystem.Play(includeChildren);

            }
        }

        //sfx/////////////////////////////////////

        
        if (usesfx == true)
        {
           // Debug.Log("asddsfasdas");
            if (audioTransform != null)
            {
                //Debug.Log("notnull");
                audio.transform.SetPositionAndRotation(@object.transform.position, @object.transform.rotation);
                if (startOn == true)
                {
                    //Debug.Log("starton");
                    //audioSystem[0].PlayDelayed(start_delay);
                    int clip = Random.Range(0, startClip.Length);
                    audioSystem[0].PlayOneShot(startClip[clip],startClipVol);
                   
                    
                }
                if (loopOn == true)
                {
                    //Debug.Log("loopon");
                    int clip = Random.Range(0, loopClip.Length);
                    audioSystem[1].PlayOneShot(loopClip[clip],loopClipVol);



                }
                

            }
            else
            {
                GameObject soundInstance = Instantiate(audio, @object.transform.position, @object.transform.rotation);
                audioTransform = soundInstance.transform; 
               // Debug.Log("instantiate");
                if (startOn == true)
                {
                    //Debug.Log("playinstantiate start");
                    audioSystem = soundInstance.GetComponents<AudioSource>();
                    audio.transform.SetPositionAndRotation(@object.transform.position, @object.transform.rotation);
                    int clip = Random.Range(0, startClip.Length);
                    audioSystem[0].PlayOneShot(startClip[clip], startClipVol);
                }
                if (loopOn == true)
                {
                    //Debug.Log("play instantiate loop");
                    audioSystem = soundInstance.GetComponents<AudioSource>();
                    audio.transform.SetPositionAndRotation(@object.transform.position, @object.transform.rotation);
                    int clip = Random.Range(0, loopClip.Length);
                    audioSystem[1].PlayOneShot(loopClip[clip], loopClipVol);
                }


            }

        }






    }

    //OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
		
	
		
        timer = +Time.deltaTime;
        
		if (particleSystem != null) {
			if (follow == true) {
			
				particleSystem.transform.SetPositionAndRotation (@object.transform.position, @object.transform.rotation);
			}
		}

			if (followaudio == true) {
			if (audioSystem [0] != null) {
				audioSystem [0].transform.SetPositionAndRotation (@object.transform.position, @object.transform.rotation);
			}
			if (audioSystem [1] != null) {
				audioSystem [1].transform.SetPositionAndRotation (@object.transform.position, @object.transform.rotation);
			}
			if (audioSystem [2] != null) {
				audioSystem [2].transform.SetPositionAndRotation (@object.transform.position, @object.transform.rotation);
			}
			}
				

    }

    // This will be called once the animator has transitioned out of the state.
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
		if (particleSystem != null) {
			// When leaving the special move state, stop the particles.
			if (vfxOn == true) {
				particleSystem.Stop (includeChildren);
			}
			if (usesfx == true) {
				if (start_fade_On == true) {
					audioSystem [0].GetComponent<audiosourcefade> ().FadeOutSound (audioSystem [0], start_fade);
				}
				/* else
            {
                audioSystem[0].Stop();
            }
            */
				if (loop_fade_On == true) {
					audioSystem [1].GetComponent<audiosourcefade> ().FadeOutSound (audioSystem [1], loop_fade);
				}
				/*else
            {
                audioSystem[1].Stop();
            }*/
				if (timer >= stopENDafter) {
					timer = 0;
					if (end_fade_On == true) {
						audioSystem [2].GetComponent<audiosourcefade> ().FadeOutSound (audioSystem [2], end_fade);
					}
					/* else
                {
                    audioSystem[2].Stop();
                }*/
				}
        

				if (audioTransform != null) {
					audio.transform.SetPositionAndRotation (@object.transform.position, @object.transform.rotation);
					if (endOn == true) {
						//Debug.Log("play end");
                    
						int clip = Random.Range (0, endClip.Length);
						audioSystem [2].PlayOneShot (endClip [clip], endClipVol);


					}
				} else {
					GameObject soundInstance = Instantiate (audio, @object.transform.position, @object.transform.rotation);
					audioTransform = soundInstance.transform;
					//Debug.Log("instanciando no end");
					if (endOn == true) {
						// Debug.Log("play instantiate end");
						audioSystem = soundInstance.GetComponents<AudioSource> ();
						audio.transform.SetPositionAndRotation (@object.transform.position, @object.transform.rotation);
						int clip = Random.Range (0, endClip.Length);
						audioSystem [2].PlayOneShot (endClip [clip], endClipVol);
					}


				}

			}
		}

        }
       

    }

    