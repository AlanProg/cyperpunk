// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Harbinger/UIglow"
{
	Properties
	{
		_Mask("Mask", 2D) = "white" {}
		_OuterGlow("Outer Glow", Color) = (0,0,0,0)
		_InnerGlow("Inner Glow", Color) = (0,0,0,0)
		_InnerPower("Inner Power", Float) = 0
		_OuterPower("Outer Power", Float) = 0
		_Opacity("Opacity", Range( 0 , 3)) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "AlphaTest+5000" "IsEmissive" = "true"  }
		Cull Back
		Blend SrcAlpha OneMinusSrcAlpha
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Unlit keepalpha noshadow 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float4 _InnerGlow;
		uniform float _InnerPower;
		uniform float4 _OuterGlow;
		uniform float _OuterPower;
		uniform sampler2D _Mask;
		uniform float4 _Mask_ST;
		uniform float _Opacity;


		float3 HSVToRGB( float3 c )
		{
			float4 K = float4( 1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0 );
			float3 p = abs( frac( c.xxx + K.xyz ) * 6.0 - K.www );
			return c.z * lerp( K.xxx, clamp( p - K.xxx, 0.0, 1.0 ), c.y );
		}


		float3 RGBToHSV(float3 c)
		{
			float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
			float4 p = lerp( float4( c.bg, K.wz ), float4( c.gb, K.xy ), step( c.b, c.g ) );
			float4 q = lerp( float4( p.xyw, c.r ), float4( c.r, p.yzx ), step( p.x, c.r ) );
			float d = q.x - min( q.w, q.y );
			float e = 1.0e-10;
			return float3( abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
		}

		inline fixed4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return fixed4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float3 hsvTorgb20 = RGBToHSV( _InnerGlow.rgb );
			float3 hsvTorgb18 = HSVToRGB( float3(hsvTorgb20.x,hsvTorgb20.y,_InnerPower) );
			float3 hsvTorgb21 = RGBToHSV( _OuterGlow.rgb );
			float3 hsvTorgb19 = HSVToRGB( float3(hsvTorgb21.x,hsvTorgb21.y,_OuterPower) );
			float2 uv_Mask = i.uv_texcoord * _Mask_ST.xy + _Mask_ST.zw;
			float4 tex2DNode3 = tex2D( _Mask, uv_Mask );
			float3 lerpResult4 = lerp( hsvTorgb18 , hsvTorgb19 , tex2DNode3.r);
			o.Emission = lerpResult4;
			o.Alpha = ( tex2DNode3.a * _Opacity );
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14101
165;249;1317;824;1723.035;393.0616;2.023786;True;True
Node;AmplifyShaderEditor.ColorNode;6;-950.3055,-207.5899;Float;False;Property;_InnerGlow;Inner Glow;3;0;Create;0,0,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;7;-973.7051,29.00994;Float;False;Property;_OuterGlow;Outer Glow;2;0;Create;0,0,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RGBToHSVNode;20;-625.8065,-280.3898;Float;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;14;-516.1049,-42.48986;Float;False;Property;_InnerPower;Inner Power;4;0;Create;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RGBToHSVNode;21;-718.1066,47.21016;Float;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;13;-712.4044,214.9102;Float;False;Property;_OuterPower;Outer Power;5;0;Create;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.HSVToRGBNode;18;-384.0051,-255.6898;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SamplerNode;3;-725.4059,307.2099;Float;True;Property;_Mask;Mask;1;0;Create;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;26;-345.4719,371.2055;Float;False;Property;_Opacity;Opacity;6;0;Create;1;0;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.HSVToRGBNode;19;-462.0049,71.91022;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;22;-191.99,228.2003;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;4;-198.9055,-6.089985;Float;False;3;0;FLOAT3;0.0,0,0,0;False;1;FLOAT3;0.0,0,0;False;2;FLOAT;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;10;0,0;Float;False;True;2;Float;ASEMaterialInspector;0;0;Unlit;Harbinger/UIglow;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;Custom;0.5;True;False;5000;True;Transparent;AlphaTest;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;False;2;SrcAlpha;OneMinusSrcAlpha;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;0;0;False;0;0;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;20;0;6;0
WireConnection;21;0;7;0
WireConnection;18;0;20;1
WireConnection;18;1;20;2
WireConnection;18;2;14;0
WireConnection;19;0;21;1
WireConnection;19;1;21;2
WireConnection;19;2;13;0
WireConnection;22;0;3;4
WireConnection;22;1;26;0
WireConnection;4;0;18;0
WireConnection;4;1;19;0
WireConnection;4;2;3;0
WireConnection;10;2;4;0
WireConnection;10;9;22;0
ASEEND*/
//CHKSM=C96009BDCAAC3171495DAA1399B1D3D09BF03916