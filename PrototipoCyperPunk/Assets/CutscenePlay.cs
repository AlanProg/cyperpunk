﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class CutscenePlay : MonoBehaviour {

    public PlayableDirector timeline;
    private bool alreadyPlayed = false;
    public Animator animator1;
    public Animator animator2;

    // Use this for initialization
    void Start () {
        timeline = timeline.GetComponent<PlayableDirector>();
        animator1.SetBool("Activated", false);
        animator2.SetBool("Activated", false);


    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player" && alreadyPlayed == false)
        {
            timeline.Play();
            alreadyPlayed = true;
            animator1.SetBool("Activated", true);
            animator2.SetBool("Activated", true);

        }
    }


}
