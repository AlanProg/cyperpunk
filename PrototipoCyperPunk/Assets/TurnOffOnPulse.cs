﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOffOnPulse : MonoBehaviour {
	public GameObject thingtoturnoff;
	public bool isoff = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (isoff == true) { 
			thingtoturnoff.SetActive (false);
		} else {
			if (!thingtoturnoff.activeSelf) {
				thingtoturnoff.SetActive (true);
			}
		}

	}
}
