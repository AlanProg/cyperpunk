﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JammerButton : MonoBehaviour {
	public GameObject target;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D col){
		if (col.tag == "Jammer") {
			target.SendMessage ("JammerPlatUpdate");
			col.SendMessage ("PlayDeathParticle");
		}
	}
}
