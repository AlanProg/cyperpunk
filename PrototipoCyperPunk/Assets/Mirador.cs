﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mirador : MonoBehaviour {
    private Vector3 targetpos;
    private GameObject alvo;
    public bool isplaying;
 

    // Use this for initialization
    void Start () {
        isplaying = true;
        alvo = GameObject.FindGameObjectWithTag("Alvo");
        
       
    }
	
	// Update is called once per frame
	void Update () {
        
        if (isplaying)
         {
            
            targetpos = alvo.transform.position;
            OlharAlvo();
       
       
         }
        
    }

    void OlharAlvo()
    {
        //transform.right = alvo - transform.position;


        Vector3 difference = targetpos - transform.position;
        float rotationZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0.0f, 0.0f, rotationZ);



    }

   

    void StopLooking()
    {
        isplaying = false;

      
    }
}
