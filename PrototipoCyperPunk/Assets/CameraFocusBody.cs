﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFocusBody : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D col){
		if (col.gameObject.tag == "Player") {
			transform.parent.SendMessage ("EnterCamera");
		}
	}

	void OnTriggerExit2D(Collider2D col){
		if (col.gameObject.tag == "Player") {
			transform.parent.SendMessage ("ExitCamera");
		}
	}
}
