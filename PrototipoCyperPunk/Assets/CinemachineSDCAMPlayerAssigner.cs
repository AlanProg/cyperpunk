﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CinemachineSDCAMPlayerAssigner : MonoBehaviour {
    private Animator animator;
    private CinemachineStateDrivenCamera cinemachinecam;
    private Transform playertransf;
    void Start ()
    {
        cinemachinecam = GetComponent<CinemachineStateDrivenCamera>();

        animator = GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>();
        cinemachinecam.m_AnimatedTarget = animator;
        
        playertransf = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        cinemachinecam.m_Follow = playertransf;

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
