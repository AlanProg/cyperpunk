﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortaJammer : MonoBehaviour {
	private Vector3 open, closed;
	private bool state = false;
	public bool closeOnCooldown = false;
	private float cooldown;
	public float maxcooldown = 10;
	public AudioClip src;
	public GameObject porta;
	private Color32 originalCol;
	public SpriteRenderer spr;
	// Use this for initialization
	void Start () {
		originalCol = spr.color;
		closed = transform.position;
		open = new Vector3 (closed.x, closed.y - 4.5f, closed.z);
	}
	
	// Update is called once per frame
	void Update () {
		
		if (state == true) {
			porta.transform.position = Vector3.MoveTowards (porta.transform.position, open, 7 * Time.deltaTime);
			spr.color = originalCol;
		
		} else {
			porta.transform.position = Vector3.MoveTowards (porta.transform.position, closed, 7 * Time.deltaTime);
			spr.color = Color.red;
	
		}
		if (cooldown <= 0 && closeOnCooldown) {
			state = false;
		}
		if (cooldown > 0) {
			cooldown -= 1 * Time.deltaTime;
		}
	}
		void JammerPlatUpdate(){
		state = true;
		cooldown = maxcooldown; 
			}

}