﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortasProximidade : MonoBehaviour {
	private Vector3 open, closed;
	private bool state,isMoving = false;
	public AudioClip src;
	public GameObject porta;
	// Use this for initialization
	void Start () {
		closed = transform.position;
		open = new Vector3 (closed.x, closed.y - 4.5f, closed.z);
	}
	
	// Update is called once per frame
	void Update () {
	if(isMoving == false){
			
		if (Vector3.Distance (Global.PlayerAcess.transform.position, transform.position) <= 7f) {

			if (state == false) {
				state = true;
				isMoving = true;
					AudioSource.PlayClipAtPoint(src,transform.position,0.4f);
				}
		} else {
				if (state == true) {
					state = false;
					isMoving = true;
					AudioSource.PlayClipAtPoint(src,transform.position,0.4f);

				}
		}

	}if (state == true) {
			porta.transform.position = Vector3.MoveTowards (porta.transform.position, open, 7* Time.deltaTime);
			if (porta.transform.position == open) {
				isMoving = false;
			}
		} else {
			porta.transform.position = Vector3.MoveTowards (porta.transform.position, closed, 7 * Time.deltaTime);
			if (porta.transform.position == closed) {
				isMoving = false;
			}
		}


}
}