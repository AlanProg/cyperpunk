﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class ApoloCutsceneTriggerInitial : MonoBehaviour {
	private bool hasPlayed = false;
	public PlayableDirector cutscene;
	public GameObject apolo;
	public bool triggerOnApolo = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (triggerOnApolo == true) {
			triggerOnApolo = false;
			apolo.SetActive (true);
		}
	}

	void OnTriggerEnter2D(Collider2D col){
		if (col.tag == "Player") {
			if (!hasPlayed) {
				hasPlayed = true;
				cutscene.Play ();
			}
		}
	}
}
